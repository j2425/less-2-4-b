#!/bin/bash

echo "PROJECT_ID: $MYPROJECT"
echo "$1, $2, $3, $4"
export MYPIPE=$(curl -s --request POST \
	--header "PRIVATE-TOKEN: $MY_PR_TOKEN" \
	--header "Content-Type: application/json" \
	--data '{ "ref": "main", "variables": [ {"key": "NGINX_PORT", "value": "'$3'"},  {"key": "NGINX_IMAGE_TAG", "value": "'$2'"}, {"key": "APP_IMAGE_TAG", "value": "'$1'"}, {"key": "JOB_ID", "value": "'$4'"} ] }' \
	"https://gitlab.com/api/v4/projects/$MYPROJECT/pipeline" | jq '.id')
echo "MYPIPE: $MYPIPE"
export MYJOB=$(curl -s --header "PRIVATE-TOKEN: $MY_PR_TOKEN" "https://gitlab.com/api/v4/projects/$MYPROJECT/pipelines/$MYPIPE/jobs" | jq '.[0].id')
echo "MYJOB: $MYJOB"
curl --request POST --header "PRIVATE-TOKEN: $MY_PR_TOKEN" "https://gitlab.com/api/v4/projects/$MYPROJECT/jobs/$MYJOB/play"
