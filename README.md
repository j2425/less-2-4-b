# Запуск Django приложения в docker-compose

1. Клонировать проект

2. Запустить compose
`docker-compose up --build -d`

3. Выполнить миграцию
`docker-compose exec web python manager.py migrate --no-input`
возможно миграция сделана уже...

4. Открываем браузер http://localhost:8000 или http://{host-ip}:8000
